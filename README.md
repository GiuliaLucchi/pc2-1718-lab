# README #

Gruppo: Giulia Lucchi, Luca Polverelli, Stefano Bernagozzi
Task: The verifier

Implementazione: 
Abbiamo filtrato dalla lista di path quelli nei quali, all'interno di almeno uno dei multi set che lo compongono, 
sono presenti contemporaneamente i token p6,p7 o p7,p7 che indicano rispettivamente la presenza di readers e writer o pi� writer. 
Per fare ci� abbiamo utilizzato i metodi filter, exists e matches.

L'output � dato da tutte le liste che NON soddisfano la mutua esclusione, in questo caso, 
siccome la propriet� � soddisfatta, non verr� mostrata nessuna lista.