package pc.modelling

object PetriNet {

  // pre-conditions, effects, inhibition
  type PetriNet[P] = Set[(MSet[P],MSet[P],MSet[P])]

  def toPartialFunction[P](pn: PetriNet[P]): PartialFunction[MSet[P],Set[MSet[P]]] =
    {case m => for ((cond,eff,inh)<-pn; if (m disjoined inh); out <- m extract cond) yield out union eff }

  def toSystem[P](pn: PetriNet[P]): System[MSet[P]] = System.ofFunction{ case m =>
    for ((cond,eff,inh)<-pn;
         if (m disjoined inh);
         out <- m extract cond) yield out union eff
  }

  def apply[P](transitions: (MSet[P],MSet[P],MSet[P])*): PetriNet[P] = transitions.toSet

  // Syntactic sugar to write transitions as:  (a,b,c) ~~> (d,e)
  implicit final class LeftTransitionRelation[A](private val self: MSet[A]){
    def ~~> (y: MSet[A]): Tuple3[MSet[A], MSet[A], MSet[A]] = Tuple3(self, y, MSet[A]())
  }
  // Syntactic sugar to write transitions as:  MSet(a,b,c) ~~> MSet(d,e) ~~ MSet(f)
  implicit final class RightTransitionRelation[A](private val self: Tuple3[MSet[A],MSet[A],MSet[A]]){
    def ~~~ (z: MSet[A]): Tuple3[MSet[A], MSet[A],MSet[A]] = Tuple3(self._1, self._2, z)
  }
}


object TryPN extends App {
  // Specification of my data-type for states
  object place extends Enumeration {
    val n,t,c = Value
  }
  type Place = place.Value
  import place._
  import MSet._
  import PetriNet._

  // TODO: support syntax (n) ~~> (t,t)
  val pn = PetriNet[Place](
    MSet(n) ~~> MSet(t),
    MSet(t) ~~> MSet(c) ~~~ MSet(c),
    MSet(c) ~~> MSet())

  val system = toSystem(pn)

  println(system.paths((n,n,n),7).toList.mkString("\n"))
}


object RwPN extends App {
  // Specification of my data-type for states
  object place extends Enumeration {
    val p1,p2,p3,p4,p5,p6,p7 = Value
  }
  type Place = place.Value
  import place._
  import MSet._
  import PetriNet._

  val pn = PetriNet[Place](
    MSet(p1) ~~> MSet(p2),
    MSet(p2) ~~> MSet(p3),
    MSet(p2) ~~> MSet(p4),
    MSet(p3,p5) ~~> MSet(p5,p6),
    MSet(p6) ~~> MSet(p1),
    MSet(p4,p5) ~~> MSet(p7) ~~~ MSet(p6),
    MSet(p7) ~~> MSet(p1,p5))

  val system = toSystem(pn)

  println(system.paths((p1,p1,p1,p5),20).toList.filter( list => list.exists( mSet =>
    mSet.matches(MSet(p6, p7)) || mSet.matches(MSet(p7, p7)))).mkString("\n"))
  println("Done")
}